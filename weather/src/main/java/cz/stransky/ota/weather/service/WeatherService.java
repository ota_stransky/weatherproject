package cz.stransky.ota.weather.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.stransky.ota.weather.service.WeatherService;
import cz.stransky.ota.weather.connector.WunderGroundConnector;
import cz.stransky.ota.weather.domain.City;
import cz.stransky.ota.weather.dto.WeatherInfoDto;
import cz.stransky.ota.weather.dto.WunderGroundDto;
import cz.stransky.ota.weather.dto.WunderGroundObservationsDto;

@Service
public class WeatherService {

	private static final Logger LOG = LoggerFactory.getLogger(WeatherService.class);
	
	private WunderGroundConnector connector;
	
	public WeatherService() {
		connector = new WunderGroundConnector();
	}
	
	public Collection<WeatherInfoDto> getWeather() {
		LOG.debug("called getWeather()");
		List<WeatherInfoDto> weathers = new ArrayList<>();
		for(City city: City.values()) {
			WeatherInfoDto dto = getWeatherForCity(city);
			weathers.add(dto);
		}
		return weathers;
	}
	
	public WeatherInfoDto getWeatherForCity(City city) {
		WunderGroundDto wunderGroundDto = connector.getWeatherForCity(city);
		return transformDtos(wunderGroundDto.getCurrent_observation());
	}
	
	private WeatherInfoDto transformDtos(WunderGroundObservationsDto wunderGroundObservationsDto) {
		WeatherInfoDto dto = new WeatherInfoDto();
		dto.setLocation(wunderGroundObservationsDto.getDisplay_location().getCity());
		dto.setCelsius(wunderGroundObservationsDto.getTemp_c());
		dto.setHumadity(wunderGroundObservationsDto.getRelative_humidity());
		dto.setTimestamp(wunderGroundObservationsDto.getLocal_time_rfc822());
		dto.setWeatherDescription(wunderGroundObservationsDto.getWeather());
		dto.setWindDescription(wunderGroundObservationsDto.getWind_string());
		dto.setWindDirection(wunderGroundObservationsDto.getWind_dir());
		dto.setWindSpeed(wunderGroundObservationsDto.getWind_kph() * 3600);
		return dto;
	}
}

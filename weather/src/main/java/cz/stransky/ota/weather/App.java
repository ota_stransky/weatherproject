package cz.stransky.ota.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App 
{
	/**
	 * Start of the REST service
	 * @param args
	 */
    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
    }
}

package cz.stransky.ota.weather.controller;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.stransky.ota.weather.domain.City;
import cz.stransky.ota.weather.dto.WeatherInfoDto;
import cz.stransky.ota.weather.exception.CityException;
import cz.stransky.ota.weather.exception.UnAuthorisedException;
import cz.stransky.ota.weather.service.AuthorisationService;
import cz.stransky.ota.weather.service.WeatherService;


@RestController
public class WeatherController {

	private static final Logger LOG = LoggerFactory.getLogger(WeatherController.class);
	
	@Autowired 
	private WeatherService weatherService;
	
	@Autowired
	private AuthorisationService authorisationService;
	
	@RequestMapping("/weather")
	public Collection<WeatherInfoDto> getWeather(@RequestHeader(value = "Authorization") String authorization) {
		LOG.debug("called getWeather()");
		
		if(!authorisationService.authorise(authorization)) {
			throw new UnAuthorisedException();
		}
		return weatherService.getWeather();
	}
	
	@RequestMapping("/weather/{city}")
	public WeatherInfoDto getWeatherForCity(@PathVariable String city, 
			@RequestHeader(value = "Authorization") String authorization ) {
		LOG.debug("called getWeatherForCity()");
		
		if(!authorisationService.authorise(authorization)) {
			throw new UnAuthorisedException();
		}
		
		City cityEnum;
		try {
			cityEnum = City.valueOf(city.toUpperCase());
		} catch (IllegalArgumentException e) {
			LOG.error("Mapping String to enum");
			throw new CityException();
		}
		
		return weatherService.getWeatherForCity(cityEnum);
	}
}

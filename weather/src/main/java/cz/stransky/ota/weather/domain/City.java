package cz.stransky.ota.weather.domain;

public enum City {
	OSTRAVA("CZ"), 
	VILNIUS("LT"), 
	OSLO("NO"), 
	STOCKHOLM("SE"), 
	HELSINKI("FI");
	
	private final String iso;
	
	City(String iso) {
		this.iso = iso;
	}

	public String getIso() {
		return this.iso;
	}
}

package cz.stransky.ota.weather.dto;

public class WeatherInfoDto {
	private String location;
	private String timestamp;
	private Double celsius;
	private String humadity;
	private Double windSpeed;
	private String windDirection;
	private String weatherDescription;
	private String windDescription;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public Double getCelsius() {
		return celsius;
	}
	public void setCelsius(Double celsius) {
		this.celsius = celsius;
	}
	public String getHumadity() {
		return humadity;
	}
	public void setHumadity(String humadity) {
		this.humadity = humadity;
	}
	public Double getWindSpeed() {
		return windSpeed;
	}
	public void setWindSpeed(Double windSpeed) {
		this.windSpeed = windSpeed;
	}
	public String getWindDirection() {
		return windDirection;
	}
	public void setWindDirection(String windDirection) {
		this.windDirection = windDirection;
	}
	public String getWindDescription() {
		return windDescription;
	}
	public void setWindDescription(String windDescription) {
		this.windDescription = windDescription;
	}
	public String getWeatherDescription() {
		return weatherDescription;
	}
	public void setWeatherDescription(String weatherDescription) {
		this.weatherDescription = weatherDescription;
	}
	
}
